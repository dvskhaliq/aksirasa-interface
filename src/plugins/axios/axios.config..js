import axios from "axios";

export default function setupAxiosInterceptors() {
  const root = process.env.VUE_APP_NEST_BASE_URL;

  axios.defaults.baseURL = root;

  axios.interceptors.request.use(
    (config) => {
      // IN PRODUCTION THIS SETTING SHOULD OPEN
      // config.headers.set('Access-Control-Allow-Origin', '*')
      // config.headers.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
      // config.headers.set('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT,HEAD' )
      // config.headers.set('Access-Control-Allow-Credentials', true)
      // config.headers.set('X-Requested-With', 'XMLHttpRequest')
      const token = localStorage.getItem("token");

      if (token) {
        config.baseURL = root;
        config.headers["Authorization"] = `Bearer ${token}`;
        // config.headers["Content-Type"] = "application/json";
      }

      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  //   axios.interceptors.response.use(
  //     (response) => {
  //       // Melakukan sesuatu sebelum respons diterima
  //       // Contoh: Memperbarui token autentikasi jika sudah kedaluwarsa
  //       const token = response.headers.authorization;
  //       if (token) {
  //         localStorage.setItem("token", token);
  //       }
  //       return response;
  //     },
  //     (error) => {
  //       // Melakukan sesuatu jika terjadi kesalahan pada respons
  //       // Contoh: Mengalihkan ke halaman error jika status respons adalah 500
  //       if (error.response.status === 500) {
  //         window.location.href = "/error";
  //       }
  //       return Promise.reject(error);
  //     }
  //   );
}
