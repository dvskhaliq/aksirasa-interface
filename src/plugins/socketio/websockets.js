import io from "socket.io-client";

// const getToken = () => {
//   return localStorage.getItem("token");
// };

// const AuthconnectToWebSocket = (token) => {
//   const NestGateWay = process.env.VUE_APP_NEST_WEBSOCKET_GATEWAY;
//   const socket = io(NestGateWay, {
//     auth: {
//       token: `Bearer ${token}`,
//     },
//   });

//   socket.on("connect", () => {
//     console.log("Terhubung ke server WebSocket");
//   });

//   socket.on("disconnect", () => {
//     console.log("Koneksi terputus, mencoba menghubungkan kembali...");
//     setTimeout(() => {
//       connectToWebSocket(token);
//     }, 5000);
//   });

//   socket.on("products", (data) => {
//     console.log("Produk baru:", data);
//   });

//   socket.on("yayasan_info", (data) => {
//     console.log("Data yayasan info:", data);
//     // Emit event or update a store with the received data
//   });

//   return socket;
// };

const connectToWebSocket = () => {
  const NestGateWay = process.env.VUE_APP_NEST_WEBSOCKET_GATEWAY;
  const socket = io(NestGateWay); // Ganti dengan URL WebSocket server Anda

  // Listener untuk event koneksi berhasil
  socket.on("connect", () => {
    console.log("Terhubung ke server WebSocket");
  });

  // Listener untuk event koneksi terputus
  socket.on("disconnect", () => {
    setTimeout(() => {
      socket.connect();
    }, 5000); // Ganti dengan waktu yang Anda inginkan
  });

  return socket;
};

// const token = getToken();
const socket = connectToWebSocket();
// let socket = null;
// if (token) {
//   socket = AuthconnectToWebSocket(token);
// } else {

// }

export default socket;
