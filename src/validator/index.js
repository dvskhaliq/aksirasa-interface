export function noSpecialCharacters(value) {
  if (!value) return true; // Biarkan kosong karena validasi wajib
  // Gunakan regular expression untuk memeriksa apakah ada karakter khusus
  return /^[a-zA-Z\s]*$/.test(value) ? true : false;
}
