const charityRoutes = [
  {
    path: "/charity",
    name: "charity",
    component: () => import("@/layouts/DefaultLayouts.vue"),
    children: [
      {
        name: "charity-catalog",
        path: "catalog",
        component: () => import("@/views/CatalogCharityView.vue"),
        meta: {
          requiresAuth: false,
        },
      },
      {
        name: "charity-detail",
        path: "charity-detail/:id",
        component: () => import("@/views/CharityDetailView.vue"),
        meta: {
          requiresAuth: false,
        },
      },
    ],
  },
];

export default charityRoutes;
