const usersRoutes = [
  {
    path: "/users",
    name: "users",
    component: () => import("@/layouts/DefaultLayouts.vue"),
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        name: "profile",
        path: "profile",
        component: () => import("@/views/UserInfoView.vue"),
        children: [
          {
            name: "profile-info",
            path: "profile-info",
            component: () => import("@/components/users/FormUserProfile.vue"),
          },
        ],
      },
      {
        name: "yayasan",
        path: "yayasan",
        component: () => import("@/views/UserInfoView.vue"),
        children: [
          {
            name: "yayasan-info",
            path: "yayasan-info",
            component: () =>
              import("@/components/users/FormUserOrganization.vue"),
          },
        ],
      },
    ],
  },
];

export default usersRoutes;
