const authRoutes = [
  {
    path: "/auth",
    name: "auth",
    component: () => import("@/layouts/BlankLayouts.vue"),
    children: [
      {
        name: "auth-signin",
        path: "signin",
        component: () => import("@/views/SigninView.vue"),
        meta: {
          requiresAuth: false,
        },
      },
      {
        name: "auth-signup",
        path: "signup",
        component: () => import("@/views/SignupView.vue"),
        meta: {
          requiresAuth: false,
        },
      },
      {
        name: "register-yayasan",
        path: "regist",
        component: () => import("@/views/RegistORG.vue"),
        meta: {
          requiresAuth: true,
        },
      },
    ],
  },
];

export default authRoutes;
