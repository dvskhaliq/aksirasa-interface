const activityRoutes = [
  {
    path: "/activity",
    name: "activity",
    component: () => import("@/layouts/DefaultLayouts.vue"),
    meta: {
      requiresAuth: false,
    },
    children: [
      {
        name: "activity-catalog",
        path: "catalog",
        component: () => import("@/views/CatalogActivityView.vue"),
      },
    ],
  },
];

export default activityRoutes;
