const mainHome = [
  {
    path: "/",
    name: "home",
    component: () => import("@/layouts/DefaultLayouts.vue"),
    meta: {
      requiresAuth: false,
    },
    children: [
      {
        name: "mainhome",
        path: "",
        component: () => import("@/views/MainView.vue"),
      },
    ],
  },
];

export default mainHome;
