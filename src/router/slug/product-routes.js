const productRoutes = [
  {
    path: "/product",
    name: "product",
    component: () => import("@/layouts/DefaultLayouts.vue"),
    meta: {
      requiresAuth: false,
    },
    children: [
      {
        name: "product-catalog",
        path: "catalog",
        component: () => import("@/views/CatalogProductView.vue"),
      },
      // {
      //   name: "auth-signup",
      //   path: "signup",
      //   component: () => import("@/views/SignupView.vue"),
      // },
    ],
  },
];

export default productRoutes;
