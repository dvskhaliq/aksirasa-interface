import { createRouter, createWebHistory } from "vue-router";
import mainHome from "./slug/home-routes";
import authRoutes from "./slug/auht-routes";
import productRoutes from "./slug/product-routes";
import charityRoutes from "./slug/charity-routes";
import activityRoutes from "./slug/activity-routes";
import usersRoutes from "./slug/users-routes";
import checkTokenMiddleware from "./middleware/CheckToken.middleware";

const routes = [
  ...mainHome,
  ...authRoutes,
  ...productRoutes,
  ...charityRoutes,
  ...activityRoutes,
  ...usersRoutes,
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    checkTokenMiddleware()(to, from, next);
  } else {
    next();
  }
});
export default router;
