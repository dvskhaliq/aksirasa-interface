export default function checkTokenMiddleware() {
  return async function (to, from, next) {
    // Periksa token dan loginId tersimpan di localStorage
    const token = localStorage.getItem("token");
    if (token) {
      // Jika token ada, jalankan Action Refresh
      return next();
    } else {
      // Jika token tidak ada, redirect ke halaman login
      return next("/auth/signin");
    }
  };
}
