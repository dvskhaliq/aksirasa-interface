import { defineStore } from "pinia";
import axios from "axios";

const userStores = defineStore("users-store", {
  state: () => {
    return {
      user_info: null,
    };
  },
  getters: {
    userInfoState: (state) => state.user_info,
  },
  actions: {
    async setUserInfo(data) {
      return this.$patch((state) => {
        state.user_info = data;
      });
    },
    async getUserInfo() {
      const url = `user/info/get`;
      const token = `Bearer ` + localStorage.getItem("token");
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      });
      return request;
    },
    async putUserInfo(formData) {
      const url = `user/info/update`;
      const token = `Bearer ` + localStorage.getItem("token");
      const form = new FormData();

      // Append form data
      form.append("pp", formData.files);
      form.append("nama_lengkap", formData.fullName);
      form.append("phone", formData.phone);
      form.append("alamat_lengkap", formData.alamat);
      const request = await axios.put(url, form, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: token,
        },
      });
      return request;
    },
  },
});

export default userStores;
