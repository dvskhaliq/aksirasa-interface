import { defineStore } from "pinia";
import axios from "axios";

const yayasanStores = defineStore("yayasan-store", {
  state: () => {
    return {
      yayasan_info: null,
      activity: [],
      product: [],
      activityFormData: {
        id: "",
        judulBerita: "",
        provinsi: "",
        kecamatan: "",
        desa: "",
        alamat: "",
        berita_acara: "",
        cover: null,
      },
      productFormData: {
        judul_product: "",
        harga: 0,
        category: "",
        description_product: "",
        cover: null,
      },
      totalActivity: 0,
      totalProduct: 0,
      editActivity: false,
      tabs: "aboutORG",
      actions: "post",
    };
  },
  getters: {
    yayasanInfoState: (state) => state.yayasan_info,
    activityState: (state) => state.activity,
    productState: (state) => state.product,
    totalActivityState: (state) => state.totalActivity,
    totalProductState: (state) => state.totalProduct,
    editActivityState: (state) => state.editActivity,
    tabsState: (state) => state.tabs,
    actionsState: (state) => state.actions,
    activityForm: (state) => state.activityFormData,
    productForm: (state) => state.productFormData,
  },
  actions: {
    async fetchYayasanInfo() {
      const url = `organization/user/charity`;
      const token = `Bearer ` + localStorage.getItem("token");
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      });
      return request;
    },
    async deleteImageProduct(id) {
      const url = `product/image/delete/${id}`;
      const token = `Bearer ` + localStorage.getItem("token");
      const request = await axios.delete(url, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      });
      return request;
    },
    async setActivityFormData(data) {
      return this.$patch((state) => {
        state.activityFormData = data;
      });
    },
    async setActions(data) {
      return this.$patch((state) => {
        state.actions = data;
      });
    },
    async setEditActivity(data) {
      return this.$patch((state) => {
        state.editActivity = data;
      });
    },
    async setTabs(data) {
      return this.$patch((state) => {
        state.tabs = data;
      });
    },
    // async setIdActivity(data) {
    //   return this.$patch((state) => {
    //     state.idActivity = data;
    //   });
    // },
    async setTotalActivity(data) {
      return this.$patch((state) => {
        state.totalActivity = data;
      });
    },
    async setActivity(data) {
      return this.$patch((state) => {
        state.activity = data;
      });
    },
    async setTotalProduct(data) {
      return this.$patch((state) => {
        state.totalProduct = data;
      });
    },
    async setProduct(data) {
      return this.$patch((state) => {
        state.product = data;
      });
    },
    async setYayasanInfo(data) {
      return this.$patch((state) => {
        state.yayasan_info = data;
      });
    },
    async setTotalItem(data) {
      return this.$patch((state) => {
        state.totalActivity = data;
      });
    },
    async getTotalItem() {
      const url = `organization/activity/user/catalog/totalitem/`;
      const token = `Bearer ` + localStorage.getItem("token");
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      });
      return request;
    },
    async putORG(formData) {
      const url = `organization/put/` + formData.id;
      const token = `Bearer ` + localStorage.getItem("token");
      const form = new FormData();
      form.append("fullname_organisasi", formData.nameYayasan);
      form.append("phone", formData.phone);
      form.append("norek", formData.nomorRekening);
      form.append("atas_nama", formData.atasNama);
      form.append("nama_bank", formData.namaBank);
      form.append("nomer_izin", formData.nomorIzin);
      form.append("website", formData.website);
      form.append("link_fb", formData.facebook);
      form.append("link_instagram", formData.instagram);
      form.append("link_twitter", formData.twitter);
      form.append("alamat", formData.alamat);
      form.append("provinsi", formData.provinsi);
      form.append("tentang_organisasi", formData.tentangORG);
      form.append("cover", formData.cover);
      form.append("logo", formData.logo);
      const request = await axios.put(url, form, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: token,
        },
      });
      return request;
    },
    async postActivitas(formData) {
      const url = `organization/aktivitas/post/` + formData.id;
      const token = `Bearer ` + localStorage.getItem("token");
      const form = new FormData();
      form.append("judul_berita", formData.judulBerita);
      form.append("lokasi_provinsi", formData.provinsi);
      form.append("lokasi_kelurahan", formData.desa);
      form.append("lokasi_kecamatan", formData.kecamatan);
      form.append("lokasi_alamat", formData.alamat);
      form.append("berita_acara", formData.berita_acara);
      form.append("image", formData.cover);
      const request = await axios.post(url, form, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: token,
        },
      });
      return request;
    },
    async putActivitas(formData) {
      const url = `organization/activity/put/` + formData.id;
      const token = `Bearer ` + localStorage.getItem("token");
      const form = new FormData();
      form.append("judul_berita", formData.judulBerita);
      form.append("lokasi_provinsi", formData.provinsi);
      form.append("lokasi_kelurahan", formData.desa);
      form.append("lokasi_kecamatan", formData.kecamatan);
      form.append("lokasi_alamat", formData.alamat);
      form.append("berita_acara", formData.berita_acara);
      form.append("cover", formData.cover);
      const request = await axios.put(url, form, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: token,
        },
      });
      return request;
    },
    async getUserInfo() {
      const url = `user/info/get`;
      const token = `Bearer ` + localStorage.getItem("token");
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      });
      return request;
    },
  },
});
export default yayasanStores;
