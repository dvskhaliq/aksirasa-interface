import { defineStore } from "pinia";
import axios from "axios";
const FormData = require("form-data");

const loginStore = defineStore("auth-login", {
  state: () => {
    return {
      jwtToken: "",
    };
  },
  actions: {
    async refreshToken() {
      const url = `organization/refresh/token`;
      const token = `Bearer ` + localStorage.getItem("token");
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
      });
      return request;
    },
    async registerUser(data) {
      const url = `auth/signup`;
      const request = await axios.post(url, {
        headers: {
          "Content-Type": "application/json",
        },
        fullname: data.fullname,
        email: data.email,
        password: data.password,
        phone: data.phone,
      });
      return request;
    },
    async getTokenServer(email, password) {
      const url = `auth/signin`;
      const request = await axios.post(url, {
        headers: {
          "Content-Type": "application/json",
        },
        email: email,
        password: password,
      });
      return request;
    },
    async registerOrg(formData) {
      const url = `organization/register`;
      const token = `Bearer ` + localStorage.getItem("token");
      const form = new FormData();

      // Append form data
      form.append("file", formData.files);
      form.append("fullname_organisasi", formData.fullname_organisasi);
      form.append("phone", formData.phone);
      form.append("norek", formData.norek);
      form.append("atas_nama", formData.atas_nama);
      form.append("nama_bank", formData.nama_bank);
      form.append("nomer_izin", formData.nomer_izin);
      form.append("website", formData.website);
      form.append("link_fb", formData.link_fb);
      form.append("link_instagram", formData.link_instagram);
      form.append("link_twitter", formData.link_twitter);
      form.append("alamat", formData.alamat);
      form.append("provinsi", formData.provinsi);
      form.append("kode_pos", formData.kode_pos);
      form.append("tentang_organisasi", formData.tentang_organisasi);
      const request = await axios.post(url, form, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: token,
        },
      });
      return request;
    },
  },
});

export default loginStore;
