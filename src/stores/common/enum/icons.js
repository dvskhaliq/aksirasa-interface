const icons = {
  SUCCESS: "mdi - check-bold",
  ERROR: "mdi - alert-circle-outline",
  INFO: "mdi - bell",
};

export default icons;
