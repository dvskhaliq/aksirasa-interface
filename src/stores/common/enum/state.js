const ApiState = {
  GOOD: "Success Connect to RestAPI",
  LOAD: "Try Load to RestAPI",
  FAIL: "Something Error",
};

export default ApiState;
