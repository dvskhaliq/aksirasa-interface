// store.js
import { defineStore } from "pinia";
import ResponseFlag from "./enum/flag";
import icons from "./enum/icons";
import router from "@/router";

const useToastStore = defineStore("toast-store", {
  id: "toast",
  state: () => ({
    toast: {
      type: "",
      show: false,
      title: "",
      message: "",
      icons: "",
      setTimeout: 5000,
    },
    // api: ApiState.GOOD,
  }),
  getters: {
    getToast: (state) => state.toast,
  },
  actions: {
    showToast(type, title, message, timeout, icon) {
      return setTimeout(() => {
        this.toast = {
          type: type,
          show: true,
          title: title,
          message: message,
          icons: icon,
          setTimeout: timeout,
        };
      }, 1500);
    },
    GreetingCustomer() {
      this.showToast({
        type: "info",
        title: "Greeting Customer",
        message: "Selamat Datang",
        icons: icons.INFO,
        setTimeout: 3000,
      });
    },
    async removeToken(resTmp) {
      if (resTmp.status === 401) {
        localStorage.removeItem("token");
        return true;
      } else {
        return false;
      }
    },
    async httpSuccess(response) {
      const code = response.status;
      const title = `${response.statusText} ${code}`;
      if (code === 200) {
        const message = response.data.status.message;
        this.showToast({
          type: ResponseFlag.SUCCESS,
          title: `[SUCCESS] : ${title}`,
          message: `${message}`,
          icon: icons.SUCCESS,
          setTimeout: 5000,
        });
      } else {
        this.showToast({
          type: ResponseFlag.SUCCESS,
          title: `[SUCCESS] : ${title}`,
          message: `Succesfull`,
          icon: icons.SUCCESS,
          setTimeout: 5000,
        });
      }
    },
    async httpErrorHandler(httpErrorResponse) {
      const code = httpErrorResponse.status;
      const title = `${httpErrorResponse.statusText} ${code}`;
      const defaultMessage = `${httpErrorResponse.data.message}`;
      if (code === 401) {
        this.showToast({
          type: ResponseFlag.ERROR,
          title: `[UNAUTHORIZED] : ${title}`,
          message: `${defaultMessage}`,
          icon: icons.ERROR,
          setTimeout: 5000,
        });
        localStorage.removeItem("token");
        localStorage.removeItem("userInfo");
        router.push({
          name: "auth-signin",
          path: "/auth/signin",
        });
      } else if (code === 404) {
        this.showToast({
          type: ResponseFlag.ERROR,
          title: `[NOTFOUND] : ${title}`,
          message: `${defaultMessage}`,
          icon: icons.ERROR,
          setTimeout: 5000,
        });
      } else if (code === 400) {
        this.showToast({
          type: ResponseFlag.ERROR,
          title: `[BAD REQUEST] : ${title}`,
          message: `${defaultMessage}`,
          icon: icons.ERROR,
          setTimeout: 5000,
        });
      } else if (code === 408) {
        this.showToast({
          type: ResponseFlag.ERROR,
          title: `[REQUEST TIMEOUT] : ${title}`,
          message: `${defaultMessage}`,
          icon: icons.ERROR,
          setTimeout: 5000,
        });
      } else {
        this.showToast({
          type: ResponseFlag.ERROR,
          title: `[UNDEFINED] : ${title}`,
          message: `${defaultMessage}`,
          icon: icons.ERROR,
          setTimeout: 5000,
        });
      }
    },
    async formHandler(msg) {
      this.showToast({
        type: ResponseFlag.WARNING,
        title: `[WARM] : Error`,
        message: msg,
        icon: icons.ERROR,
        setTimeout: 5000,
      });
    },
    async systemErrorHandler(httpErrorResponse) {
      console.log(httpErrorResponse);
      this.showToast({
        type: ResponseFlag.ERROR,
        title: `[FATAL] : Error`,
        message: `Please contact your administrator`,
        icon: icons.ERROR,
        setTimeout: 5000,
      });
    },
    async socketErrorHandler() {
      this.showToast({
        type: ResponseFlag.ERROR,
        title: `[FATAL] : Error`,
        message: `Your connection is interrupted, You cannot connect to the server.`,
        icon: icons.ERROR,
        setTimeout: 5000,
      });
    },
  },
});

export default useToastStore;
