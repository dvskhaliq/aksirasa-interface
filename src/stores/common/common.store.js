import { defineStore } from "pinia";
import axios from "axios";

const commonStores = defineStore("common-store", {
  state: () => {
    return {
      provinces: [],
      cover: [],
      category: [],
    };
  },
  getters: {
    provinsiState: (state) => state.provinces,
    coverState: (state) => state.cover,
    categoryState: (state) => state.category,
  },
  actions: {
    async setCover(data) {
      return this.$patch((state) => {
        state.cover = data;
      });
    },
    async setProvinsi(data) {
      return this.$patch((state) => {
        state.provinces = data;
      });
    },
    async setCategoryProduct(data) {
      return this.$patch((state) => {
        state.category = data;
      });
    },
    async getCategoryProduct() {
      // const apiUrl = process.env.VUE_APP_NEST_BASE_URL;
      const url = `common-services/category/get`;
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async getProvinces() {
      // const apiUrl = process.env.VUE_APP_NEST_BASE_URL;
      const url = `common-services/provinsi/get`;
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async getHomeCover() {
      const url = `common-services/image/cover`;
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
  },
});

export default commonStores;
