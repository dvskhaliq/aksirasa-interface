import { defineStore } from "pinia";
import axios from "axios";
// import useToastStore from "../common/toast.store";

const activityStores = defineStore("activity-store", {
  state: () => {
    return {
      charityActivity: [],
      ActivityPage: 1,
      Take: 2,
      filterOnly: false,
      searchQuery: "",
      searchProvinsi: [],
      totalItem: 0,
    };
  },
  getters: {
    filter: (state) => state.filterOnly,
    take: (state) => state.Take,
    skip: (state) => state.ActivityPage,
    activityState: (state) => state.charityActivity,
    totalData: (state) => state.totalItem,
    searchKeyword: (state) => state.searchQuery,
  },
  actions: {
    //MUTTATION STATE ACTIONS
    async setfilterOnly(data) {
      return this.$patch((state) => {
        state.filterOnly = data;
      });
    },
    async setSearchQuery(data) {
      return this.$patch((state) => {
        if (data !== null) {
          state.searchQuery = data;
        } else {
          state.searchQuery = "";
        }
      });
    },
    async setSearchProvinsi(data) {
      return this.$patch((state) => {
        if (data.length !== 0) {
          state.searchProvinsi.splice(0, state.searchProvinsi.length, data);
        } else {
          state.searchProvinsi = [];
        }
      });
    },
    async setCharity(data) {
      return this.$patch((state) => {
        state.charityActivity = data;
      });
    },
    async setTotalItem(data) {
      return this.$patch((state) => {
        state.totalItem = data;
      });
    },
    //MUTTATION STATE ACTIONS
    async getOrgByParam(keyword, provinsi) {
      if (keyword && provinsi === null) {
        const url = "organization/activity/byParam";
        const request = await axios.get(url, {
          params: {
            keyword: keyword,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      } else if (keyword === null && provinsi) {
        const url = "organization/activity/byParam";
        const request = await axios.get(url, {
          params: {
            provinsi: provinsi,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      } else {
        const url = "organization/activity/byParam";
        const request = await axios.get(url, {
          params: {
            keyword: keyword,
            provinsi: provinsi,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      }
    },
    async getCharity(skip, take, order) {
      const url = "organization/activity";
      const request = await axios.get(url, {
        params: {
          page: skip,
          take: take,
          order: order,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async getTotalItem() {
      const url = "organization/activity/catalog/totalitem";
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
  },
});

export default activityStores;
