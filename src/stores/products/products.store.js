import { defineStore } from "pinia";
import axios from "axios";
// import useToastStore from "../common/toast.store";

const productStores = defineStore("product-store", {
  state: () => {
    return {
      category: [],
      ProductPage: 1,
      Take: 2,
      products: [],
      filterOnly: false,
      searchQuery: "",
      searchCategory: [],
      totalItem: 0,
    };
  },
  getters: {
    filterProduct: (state) => state.filterOnly,
    take: (state) => state.Take,
    skip: (state) => state.ProductPage,
    productState: (state) => state.products,
    categoryState: (state) => state.category,
    totalData: (state) => state.totalItem,
  },
  actions: {
    //MUTTATION STATE ACTIONS
    async setfilterOnly(data) {
      return this.$patch((state) => {
        state.filterOnly = data;
      });
    },
    async setSearchQuery(data) {
      return this.$patch((state) => {
        if (data !== null) {
          state.searchQuery = data;
        } else {
          state.searchQuery = "";
        }
      });
    },
    async setSearchCategory(data) {
      return this.$patch((state) => {
        if (data.length !== 0) {
          state.searchCategory.splice(0, state.searchCategory.length, data);
        } else {
          state.searchCategory = [];
        }
      });
    },
    async setCategory(data) {
      return this.$patch((state) => {
        state.category = data;
      });
    },
    async setProduct(data) {
      return this.$patch((state) => {
        state.products = data;
      });
    },
    async setTotalItem(data) {
      return this.$patch((state) => {
        state.totalItem = data;
      });
    },
    //MUTTATION STATE ACTIONS
    async getcategory() {
      // const apiUrl = process.env.VUE_APP_NEST_BASE_URL;
      const url = `common-services/category/get`;
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async getProductByParam(keyword, category) {
      if (keyword && category === null) {
        const url = "product/catalog/byParam";
        const request = await axios.get(url, {
          params: {
            keyword: keyword,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      } else if (keyword === null && category) {
        const url = "product/catalog/byParam";
        const request = await axios.get(url, {
          params: {
            category: category,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      } else {
        const url = "product/catalog/byParam";
        const request = await axios.get(url, {
          params: {
            keyword: keyword,
            category: category,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      }
    },
    async getProducts(page, take, order) {
      const url = "product/catalog";
      const request = await axios.get(url, {
        params: {
          page: page,
          take: take,
          order: order,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async getTotalItem() {
      const url = "product/catalog/totalitem";
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async processDataProduct(data) {
      const processedData = [];
      for (const proses of data) {
        if (proses.rating && proses.rating.length > 0) {
          let onestart = 0;
          let twostart = 0;
          let threestart = 0;
          let fourstart = 0;
          let fivestart = 0;
          let undifined = 0;
          // Count Rating Quantity
          for (let index = 0; index < proses.rating.length; index++) {
            if (proses.rating[index].product_rating === 1) {
              onestart += 1;
            } else if (proses.rating[index].product_rating === 2) {
              twostart += 1;
            } else if (proses.rating[index].product_rating === 3) {
              threestart += 1;
            } else if (proses.rating[index].product_rating === 4) {
              fourstart += 1;
            } else if (proses.rating[index].product_rating === 5) {
              fivestart += 1;
            } else {
              undifined += 1;
            }
          }
          const arr = [
            onestart,
            twostart,
            threestart,
            fourstart,
            fivestart,
            undifined,
          ];
          const sum = arr.reduce(
            (accumulator, currentValue) => accumulator + currentValue,
            0
          );
          const rata = sum / proses.rating.length;
          const singleProcessedData = {
            id: proses.id,
            yayasan_id: proses.yayasan_id,
            judul_product: proses.judul_product,
            category: proses.category,
            harga: proses.harga,
            description: proses.description_product,
            image: proses.image,
            rating: { sum: sum, rata: rata },
            owner: {
              fullname_organisasi: proses.owner.fullname_organisasi,
              provinsi: proses.owner.provinsi,
              logo: proses.owner.logo,
            },
          };
          processedData.push(singleProcessedData);
        } else {
          const singleProcessedData = {
            id: proses.id,
            yayasan_id: proses.yayasan_id,
            judul_product: proses.judul_product,
            category: proses.category,
            harga: proses.harga,
            description: proses.description_product,
            image: proses.image,
            rating: { sum: 0, rata: 0 },
            owner: {
              fullname_organisasi: proses.owner.fullname_organisasi,
              provinsi: proses.owner.provinsi,
              logo: proses.owner.logo,
            },
          };
          processedData.push(singleProcessedData);
        }
      }
      return processedData;
    },
  },
});

export default productStores;
