import { defineStore } from "pinia";
import axios from "axios";

const charityStores = defineStore("charity-store", {
  state: () => {
    return {
      details: null,
      provinces: [],
      charity: [],
      charityPage: 1,
      Take: 2,
      filterOnly: false,
      searchQuery: "",
      searchProvinsi: [],
      totalItemCharity: 0,
      isLoading: false,
    };
  },
  getters: {
    filterCharity: (state) => state.filterOnly,
    detailCharity: (state) => state.details,
    Loading: (state) => state.isLoading,
    take: (state) => state.Take,
    skip: (state) => state.charityPage,
    charityState: (state) => state.charity,
    totalData: (state) => state.totalItemCharity,
    provinsiState: (state) => state.provinces,
  },
  actions: {
    //MUTTATION STATE ACTIONS
    async setIsLoading(data) {
      return this.$patch((state) => {
        state.isLoading = data;
      });
    },
    async setfilterOnly(data) {
      return this.$patch((state) => {
        state.filterOnly = data;
      });
    },
    async setSearchQuery(data) {
      return this.$patch((state) => {
        if (data !== null) {
          state.searchQuery = data;
        } else {
          state.searchQuery = "";
        }
      });
    },
    async setSearchProvinsi(data) {
      return this.$patch((state) => {
        if (data.length !== 0) {
          state.searchProvinsi.splice(0, state.searchProvinsi.length, data);
        } else {
          state.searchProvinsi = [];
        }
      });
    },
    async setProvinsi(data) {
      return this.$patch((state) => {
        state.provinces = data;
      });
    },
    async setCharity(data) {
      return this.$patch((state) => {
        state.charity = data;
      });
    },
    async setTotalItem(data) {
      return this.$patch((state) => {
        state.totalItemCharity = data;
      });
    },
    async setDetailsItem(data) {
      return this.$patch((state) => {
        state.details = data;
      });
    },
    //MUTTATION STATE ACTIONS
    async getCharityDetail(id) {
      const url = "organization/detailcharity/" + id + "";
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
        params: {
          yayasan_id: id,
        },
      });
      return request;
    },
    async getProvinsi() {
      // const apiUrl = process.env.VUE_APP_NEST_BASE_URL;
      const url = `common-services/provinsi/get`;
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async getOrgByParam(keyword, provinsi) {
      if (keyword && provinsi === null) {
        const url = "organization/byParam";
        const request = await axios.get(url, {
          params: {
            keyword: keyword,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      } else if (keyword === null && provinsi) {
        const url = "organization/byParam";
        const request = await axios.get(url, {
          params: {
            provinsi: provinsi,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      } else {
        const url = "organization/byParam";
        const request = await axios.get(url, {
          params: {
            keyword: keyword,
            provinsi: provinsi,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        return request;
      }
    },
    async getCharity(skip, take, order) {
      const url = "organization";
      const request = await axios.get(url, {
        params: {
          page: skip,
          take: take,
          order: order,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
    async getTotalItem() {
      const url = "organization/catalog/totalitem";
      const request = await axios.get(url, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      return request;
    },
  },
});

export default charityStores;
