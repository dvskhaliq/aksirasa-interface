import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
// import Vue plugin
import vuetify from "./plugins/vuetify/vuetify";
import VueSvgInlinePlugin from "vue-svg-inline-plugin";
import { vuetifyProTipTap } from "./plugins/tiptap";

// import polyfills for IE if you want to support it
import "vue-svg-inline-plugin/src/polyfills";
// axios config
import setupAxiosInterceptors from "@/plugins/axios/axios.config.";

//Store

const pinia = createPinia();
const app = createApp(App);
app.use(router);
app.use(vuetify);
app.use(pinia);
app.use(VueSvgInlinePlugin, {
  attributes: {
    data: ["src"],
    remove: ["alt"],
  },
});
app.use(vuetifyProTipTap);
// fix warning injected property "decorationClasses" is a ref and will be auto-unwrapped
// https://github.com/ueberdosis/tiptap/issues/1719
app.config.unwrapInjectedRef = true;

setupAxiosInterceptors();
app.mount("#app");
